<?php

namespace App\Helpers;

use App\Helpers\Hightlight;

class Template
{
    public static function showItemImage($controller, $thumb, $alt)
    {
        $xhtml = sprintf(
            '<img class="thumb" src="%s" alt="%s">',
            asset('images/' . $controller . '/' . $thumb),
            $alt
        );
        return $xhtml;
    }

    public static function showItemStatus($controller, $status, $id)
    {
        $tmpStatus = config('custom.template.status');
        $status = array_key_exists($status, $tmpStatus) ? $status : 'default';
        $currentStatus = $tmpStatus[$status];
        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>',
            route($controller . '-change-status', ['status' => $status, 'id' => $id]),
            $currentStatus['class'],
            $currentStatus['name']
        );

        return $xhtml;
    }

    public static function showItemIsHome($controller, $isHome, $id)
    {
        $tmpIsHome = config('custom.template.is_home');
        $isHome = array_key_exists($isHome, $tmpIsHome) ? $isHome : 'hide';
        $currentIsHome = $tmpIsHome[$isHome];
        $xhtml = sprintf(
            '<a href="%s" type="button" class="btn btn-round %s">%s</a>',
            route($controller . '-is-home', ['is_home' => $isHome, 'id' => $id]),
            $currentIsHome['class'],
            $currentIsHome['name']
        );

        return $xhtml;
    }

    public static function showItemHistory($by, $time, $field, $searchValue)
    {
        $customFormat = config('custom.format.long_time', 'H:m:s d-m-Y');
        $xhtml = sprintf(
            '<i class="fas fa-user mr-2"></i> %s</br>
            <i class="fas fa-clock mr-2"></i> %s',
            Hightlight::show($by, $field, $searchValue),
            date($customFormat, strtotime($time))
        );
        return $xhtml;
    }

    public static function showItemOptions($controller, $id)
    {
        $tmpButtons = config('custom.template.options');
        $btnsInArea = config('custom.area.options');
        $controller = array_key_exists($controller, $btnsInArea) ? $controller : 'default';

        $xhtml = '<div class="action-box">';
        foreach ($btnsInArea[$controller] as $btn) {
            $currentBtn = $tmpButtons[$btn];
            $xhtml .= sprintf(
                '<a href="%s" type="button" class="btn btn-icon %s" data-toggle="tooltip"
                data-placement="top" data-original-title="%s">
                <i class="fas %s"></i></a>',
                route($controller . $currentBtn['route-name'], ['id' => $id]),
                $currentBtn['class'],
                $currentBtn['title'],
                $currentBtn['icon']
            );
        }

        return $xhtml . '</div>';
    }

    public static function showFilterButtons($controller, $itemsByStatus, $params)
    {
        $tmpStatus = config('custom.template.status');

        $xhtml = null;
        if (count($itemsByStatus) > 0) {
            array_unshift($itemsByStatus, [
                'status' => 'all',
                'count' => array_sum(array_column($itemsByStatus, 'count')),
            ]);
            foreach ($itemsByStatus as $item) {
                $status = array_key_exists($item['status'], $tmpStatus) ? $item['status'] : 'default';
                $currentStatus = $tmpStatus[$status];
                $classActive = ($status == $params['filter']['status']) ? 'btn-info' : 'btn-default';
                $href = route($controller) . '?filter_status=' . $item['status'];

                if ($params['search']['value'] !== '') {
                    $href .= '&search_field=' . $params['search']['field'] . '&search_value=' . $params['search']['value'];
                }

                $xhtml .= sprintf(
                    '<a href="%s" type="button"
                        class="btn %s btn-sm mr-2">
                        %s<span class="badge badge-danger ml-2">%s</span>
                    </a>',
                    $href,
                    $classActive,
                    $currentStatus['name'],
                    $item['count']
                );
            }
        }

        return $xhtml;
    }

    public static function showSearchArea($controller, $search)
    {
        $tmpSearchField = config('custom.template.search');
        $fieldInController = config('custom.area.search');
        $controller = array_key_exists($controller, $fieldInController) ? $controller : 'default';
        $searchField = in_array($search['field'], $fieldInController[$controller]) ? $search['field'] : 'name';

        $xhtml = null;
        $xhtmlField = null;
        foreach ($fieldInController[$controller] as $field) {
            $xhtmlField .= sprintf(
                '<li class="dropdown-item"><a href="javascript;" data-field="%s" class="select-field">%s</a></li>',
                $field,
                $tmpSearchField[$field]['name']
            );
        }

        $xhtml .= sprintf(
            '<div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                    <button type="button" class="btn btn-default dropdown-toggle btn-active-field" data-toggle="dropdown">
                        %s
                    </button>
                    <ul class="dropdown-menu">%s</ul>
                </div>
                <input type="text" name="search_value" value="%s" class="form-control" style="max-width: 190px;">
                <span class="input-group-append">
                    <button type="button" id="btn-clear-search" class="btn btn-primary btn-flat"><i class="fas fa-eraser mr-2"></i>Clear</button>
                </span>
                <span class="input-group-append">
                    <button type="button" id="btn-search" class="btn btn-info btn-flat"><i class="fas fa-search mr-2"></i>Search</button>
                </span>
                <input type="hidden" name="search_field" value="%s">
            </div>',
            $tmpSearchField[$searchField]['name'],
            $xhtmlField,
            $search['value'],
            $searchField
        );

        return $xhtml;
    }

    public static function showItemMenu($controller, $itemsCategory)
    {
        $xhtml = '';
        if (count($itemsCategory) > 0) {
            $xhtml .= '<div class="collapse navbar-collapse" id="navbar-menu"><ul class="nav navbar-nav ml-auto" data-in="fadeInDown" data-out="fadeOutUp">';

            foreach ($itemsCategory as $item) {
                $xhtml .= sprintf(
                    '<li class="nav-item"><a class="nav-link" href="%s">%s</a></li>',
                    route($item['slug']),
                    $item['name']
                );
            }

            $xhtml .= '</ul></div>';
        }
        return $xhtml;
    }
}
