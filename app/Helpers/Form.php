<?php

namespace App\Helpers;

class Form
{
    public static function show($elements)
    {
        $xhtml = '<div class="card-body">';

        foreach ($elements as $element)
            $xhtml .= self::formGroup($element);

        return $xhtml;
    }

    public static function formGroup($element)
    {
        $type = isset($element['type']) ? $element['type'] : 'input';

        $xhtml = null;

        switch ($type) {
            case 'input':
                $xhtml .= sprintf(
                    '<div class="form-group row">%s%s</div>',
                    $element['label'],
                    $element['input']
                );
                break;

            case 'thumb':
                $xhtml .= sprintf(
                    '<div class="form-group row">
                        %s
                        <div class="input-group %s">%s</div>
                        <p class="col-sm-7 offset-sm-3 mt-3 mb-0 p-0">%s</p>
                    </div>',
                    $element['label'],
                    config('custom.template.form_input_file.class'),
                    $element['input'],
                    $element['thumb']
                );
                break;

            case 'btn-submit':
                $xhtml .= sprintf(
                    '</div>
                    <div class="card-footer form-group row">%s</div>',
                    $element['input']
                );
                break;
        }

        return $xhtml;
    }
}
