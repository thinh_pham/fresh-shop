<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'description' => 'required',
            'link' => 'bail|required|min:5|url',
            'status' => 'bail|in:active,inactive',
            'thumb' => 'bail|required|image|max:200',
        ];
    }

    public function messages()
    {
        return [
            // 'name.required' => 'Name is required',
            // 'name.min' => 'Name must be at least :min characters',
            // 'description.required' => 'Please enter description',
        ];
    }

    public function attribute()
    {
        return [
            // 'description' => 'Description',
        ];
    }
}
