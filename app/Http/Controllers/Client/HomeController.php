<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Models\CategoryModel as CategoryModel;
use App\Models\SliderModel as SliderModel;

class HomeController extends Controller
{
    private $pathView = 'front-end.pages.home.';
    private $controllerName = 'home';
    private $params = [];
    private $model;
    private $category;
    private $slider;

    public function __construct()
    {
        $this->category = new CategoryModel();
        $this->slider = new SliderModel();
        view()->share('controller', $this->controllerName);
    }

    public function index()
    {
        $itemsCategory = $this->category->list(null, ['task' => 'front-end-list-item']);
        $itemsSlider = $this->slider->list(null, ['task' => 'front-end-list-item']);
        return view($this->pathView . 'index', [
            'itemsCategory' => $itemsCategory,
            'itemsSlider' => $itemsSlider,
        ]);
    }
}
