<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    private $pathView = 'back-end.pages.dashboard.';
    private $controllerName = 'dashboard';
    private $params = [];
    private $model;

    public function __construct() {
        view()->share('controller', $this->controllerName);
    }

    public function index()
    {
        return view($this->pathView . 'index');
    }
}
