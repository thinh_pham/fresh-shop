<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest as MainRequest;
use App\Models\CategoryModel as MainModel;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $pathView = 'back-end.pages.category.';
    private $controllerName = 'category';
    private $params = [];
    private $model;

    public function __construct()
    {
        $this->params['pagination']['item-per-page'] = 5;
        $this->model = new MainModel();
        view()->share('controller', $this->controllerName);
    }

    public function index(Request $request)
    {
        $this->params['filter']['status'] = $request->input('filter_status', 'all');
        $this->params['search']['field'] = $request->input('search_field', 'all');
        $this->params['search']['value'] = $request->input('search_value', '');

        $items = $this->model->list($this->params, ['task' => 'back-end-list-item']);
        $itemsByStatus = $this->model->countItem($this->params, ['task' => 'back-end-count-item']);

        return view($this->pathView . 'index', [
            'items' => $items,
            'itemsByStatus' => $itemsByStatus,
            'params' => $this->params,
        ]);
    }

    public function form(Request $request)
    {
        $item = null;

        if ($request->id !== '') {
            $this->params['item']['id'] = $request->id;
            $item = $this->model->getItem($this->params, ['task' => 'back-end-get-item']);
        }

        return view($this->pathView . 'form', ['item' => $item]);
    }

    public function save(MainRequest $request)
    {
        if ($request->method() == 'POST') {
            $this->params['item'] = $request->all();

            $task = 'back-end-add-item';
            $notify = 'Successfully added the element';
            if ($this->params['item']['id'] !== null) {
                $task = 'back-end-edit-item';
                $notify = 'Successful element update';
            }
            $this->model->saveItem($this->params, ['task' => $task]);
            return redirect()->route($this->controllerName)->with('nofify', $notify);
        }
    }

    public function delete(Request $request)
    {
        $this->params['item']['id'] = $request->id;
        $this->model->deleteItem($this->params, ['task' => 'back-end-delete-item']);
        return redirect()->route($this->controllerName)->with('nofify', 'Successfully deleted the element !!!');
    }

    public function status(Request $request)
    {
        $this->params['item']['status'] = $request->status;
        $this->params['item']['id'] = $request->id;
        $task = 'back-end-change-status';
        if ($request->is_home !== null) {
            $this->params['item']['is_home'] = $request->is_home;
            $task = 'back-end-is-home';
        }

        $this->model->saveItem($this->params, ['task' => $task]);
        return redirect()->route($this->controllerName)->with('nofify', 'Update was successful !!!');
    }
}
