<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CategoryModel extends Model
{
    protected $table = 'category';
    private $uploadFolder = 'category';
    // protected $primaryKey = 'id';
    public $timestamps = false;
    const CREATED_AT = 'created';
    const UPDATED_AT = 'modified';
    private $fieldAccepted = [
        'name',
        'created_by',
        'modified_by',
    ];
    private $crudNotAccepted = [
        '_token',
    ];

    function list($params = null, $options = null) {
        $result = null;

        if ($options['task'] == 'back-end-list-item') {
            $query = self::select('id', 'name', 'status', 'created', 'created_by', 'modified', 'modified_by', 'is_home', 'display');

            // filter
            if (isset($params['filter']['status']) && $params['filter']['status'] !== 'all') {
                $query->where('status', '=', $params['filter']['status']);
            }

            // search
            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                    $query = $query->where(function ($query) use ($params) {
                        foreach ($this->fieldAccepted as $field) {
                            $query->orWhere($field, 'like', "%{$params['search']['value']}%");
                        }
                    });
                } else if (in_array($params['search']['field'], $this->fieldAccepted)) {
                    $query->where($params['search']['field'], 'like', "%{$params['search']['value']}%");
                }
            }

            $result = $query->orderBy('id', 'desc')->paginate($params['pagination']['item-per-page']);
        }

        if ($options['task'] == 'front-end-list-item') {
            $query = self::select('id', 'name', 'slug', 'status', 'is_home', 'display')
                ->where('status', '=', 'active')
                ->limit(8);
            $result = $query->get()->toArray();
        }

        return $result;
    }

    public function getItem($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-get-item') {
            $result = self::select('id', 'name', 'status')
                ->where('id', $params['item']['id'])->first();
        }

        return $result;
    }

    public function countItem($params = null, $options = null)
    {
        $result = null;

        if ($options['task'] == 'back-end-count-item') {
            $query = self::select('status', DB::raw('count(id) as count'))
                ->groupBy('status');

            // search
            if ($params['search']['value'] !== '') {
                if ($params['search']['field'] == 'all') {
                } else if (in_array($params['search']['field'], $this->fieldAccepted)) {
                    $query->where($params['search']['field'], 'like', "%{$params['search']['value']}%");
                }
            }

            $result = $query->get()->toArray();
        }

        return $result;
    }

    public function saveItem($params = null, $options = null)
    {
        if ($options['task'] == 'back-end-change-status') {
            $status = $params['item']['status'] == 'active' ? 'inactive' : 'active';
            self::where('id', $params['item']['id'])->update(['status' => $status]);
        }

        if ($options['task'] == 'back-end-is-home') {
            $isHome = $params['item']['is_home'] == 'show' ? 'hide' : 'show';
            self::where('id', $params['item']['id'])->update(['is_home' => $isHome]);
        }

        if ($options['task'] == 'back-end-add-item') {
            // created_by
            $params['item']['created_by'] = 'tp754';
            $params['item']['created'] = date('Y-m-d H:i:s');

            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            self::insert($params['item']);
        }

        if ($options['task'] == 'back-end-edit-item') {
            // created_by
            $params['item']['created_by'] = 'tp754';
            $params['item']['created'] = date('Y-m-d H:i:s');

            $params['item'] = array_diff_key($params['item'], array_flip($this->crudNotAccepted));
            self::where('id', $params['item']['id'])->update($params['item']);
        }

    }

    public function deleteItem($params = null, $options = null)
    {
        if ($options['task'] == 'back-end-delete-item') {
            self::where('id', $params['item']['id'])->delete();
        }

    }
}
