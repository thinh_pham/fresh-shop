@extends('front-end.main')
@section('page-title')
    Freshshop - Home
@endsection

@section('page-content')
    <!-- Start Slider -->
    @include('front-end.pages.home.slider')
    <!-- End Slider -->

    <!-- Start Categories  -->
    @include('front-end.pages.home.categories')
    <!-- End Categories -->

    @include('front-end.pages.home.box-add-product')

    <!-- Start Products  -->
    @include('front-end.pages.home.products-box')
    <!-- End Products  -->

    <!-- Start Blog  -->
    @include('front-end.pages.home.latest-blog')
    <!-- End Blog  -->
@endsection
