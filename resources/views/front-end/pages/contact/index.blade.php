@extends('front-end.main')
@section('page-title')
    Freshshop - Contact Us
@endsection

@section('page-content')
    <!-- Start All Title Box -->
    @include('front-end.template.title-box')
    <!-- End All Title Box -->

    <!-- Start Contact Us  -->
    <div class="contact-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-sm-12">
                    @include('front-end.pages.contact.form')
                </div>
                <div class="col-lg-4 col-sm-12">
                    @include('front-end.pages.contact.info')
                </div>
            </div>
        </div>
    </div>
    <!-- End Contact Us -->
@endsection
