<table class="table">
    <thead>
        <tr>
            <th>Images</th>
            <th>Product Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Remove</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="thumbnail-img">
                <a href="#">
                    <img class="img-fluid" src="{{ asset('images/front-end/img-pro-01.jpg') }}" alt="" />
                </a>
            </td>
            <td class="name-pr">
                <a href="#">
                    Lorem ipsum dolor sit amet
                </a>
            </td>
            <td class="price-pr">
                <p>$ 80.0</p>
            </td>
            <td class="quantity-box"><input type="number" size="4" value="1" min="0" step="1"
                    class="c-input-text qty text"></td>
            <td class="total-pr">
                <p>$ 80.0</p>
            </td>
            <td class="remove-pr">
                <a href="#">
                    <i class="fas fa-times"></i>
                </a>
            </td>
        </tr>
    </tbody>
</table>
