@extends('front-end.main')
@section('page-title')
    Freshshop - Gallery
@endsection

@section('page-content')
    <!-- Start All Title Box -->
    @include('front-end.template.title-box')
    <!-- End All Title Box -->

    <!-- Start Cart  -->
    <div class="cart-box-main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-main table-responsive">
                        @include('front-end.pages.cart.list')
                    </div>
                </div>
            </div>

            <div class="row my-5">
                <div class="col-lg-8 col-sm-12"></div>
                <div class="col-lg-2 col-sm-12">
                    <div class="d-flex gr-total">
                        <h5>Total</h5>
                        <div class="ml-auto h5"> $ 388 </div>
                    </div>
                </div>
                <div class="col-lg-2 d-flex shopping-box"><a href="checkout.html" class="ml-auto btn hvr-hover">Checkout</a>
                </div>
            </div>

        </div>
    </div>
    <!-- End Cart -->
@endsection
