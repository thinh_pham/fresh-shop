@extends('front-end.main')
@section('page-title')
    Freshshop - Gallery
@endsection

@section('page-content')
    <!-- Start All Title Box -->
    @include('front-end.template.title-box')
    <!-- End All Title Box -->
    <!-- Start Shop Page  -->
    <div class="shop-box-inner">
        <div class="container">
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-sm-12 col-xs-12 shop-content-right">
                    @include('front-end.pages.product.p-box')
                </div>
				<div class="col-xl-3 col-lg-3 col-sm-12 col-xs-12 sidebar-shop-left">
                    @include('front-end.pages.product.p-category')
                </div>
            </div>
        </div>
    </div>
    <!-- End Shop Page -->
@endsection