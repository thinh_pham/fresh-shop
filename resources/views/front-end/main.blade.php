<!DOCTYPE html>
<html lang="en">
<!-- Basic -->

<head>
    <title>@yield('page-title')</title>
    @include('front-end.elements.head')
</head>

<body>
    <!-- Start Main Top -->
    {{-- @include('front-end.elements.m-top') --}}
    <!-- End Main Top -->

    <!-- Start Main Top -->
    @include('front-end.elements.m-header')
    <!-- End Main Top -->

    <!-- Start Top Search -->
    @include('front-end.elements.t-search')
    <!-- End Top Search -->

    <!-- Start Content -->
    @yield('page-content')
    <!-- End Content -->

    <!-- Start Instagram Feed  -->
    @include('front-end.template.instagram-box')
    <!-- End Instagram Feed  -->

    <!-- Start Footer  -->
    @include('front-end.elements.footer')
    <!-- End Footer  -->

    <!-- Start copyright  -->
    @include('front-end.elements.cp-right')
    <!-- End copyright  -->

    <a href="#" id="back-to-top" title="Back to top" style="display: none;">&uarr;</a>

    @include('front-end.elements.script')

</body>

</html>
