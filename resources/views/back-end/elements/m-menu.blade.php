<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
       with font-awesome or any other icon font library -->
        <li class="nav-header ml-2">GENERAL</li>
        <li class="nav-item">
            <a href="{{ route('dashboard') }}"
                class="nav-link {{ route('dashboard') == route($controller) ? 'active' : '' }}">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="#" class="nav-link">
                <i class="nav-icon far fa-envelope"></i>
                <p>
                    Mailbox
                    <span class="right badge badge-danger">New</span>
                </p>
            </a>
        </li>
        <li class="nav-header">TABLES</li>
        <li class="nav-item">
            <a href="{{ route('category') }}"
                class="nav-link {{ route('category') == route($controller) ? 'active' : '' }}">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Category
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('slider') }}"
                class="nav-link {{ route('slider') == route($controller) ? 'active' : '' }}">
                <i class="nav-icon fas fa-sliders-h"></i>
                <p>
                    Slider
                </p>
            </a>
        </li>
    </ul>
</nav>
