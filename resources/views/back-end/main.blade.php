<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('page-title')</title>
    @include('back-end.elements.head')
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        @include('back-end.elements.t-nav')
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        @include('back-end.elements.r-sidebar')

        <!-- Content Wrapper. Contains page content -->
        @yield('page-content')
        <!-- /.content-wrapper -->
        @include('back-end.elements.footer')
        
    </div>
    <!-- ./wrapper -->
    @include('back-end.elements.script')

</body>

</html>
