@if (session('nofify'))
    <div class="alert alert-success alert-dismissible mx-1" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong>{{ session('nofify') }}</strong>
    </div>
@endif
