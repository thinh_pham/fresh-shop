@if ($paginator->hasPages())
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">

            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="page-item"><span class="page-link disable">&laquo;</span></li>
            @else
                <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}">&laquo;</a></li>
            @endif
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Three Dot Separator --}}
                @if (is_string($element))
                    <li class="page-item"><span class="page-link">{{ $element }}</span></li>
                @endif

                @if (is_array($element))
                    @foreach ($element as $numberPage => $url)
                        @if ($numberPage == $paginator->currentPage())
                            <li class="page-item active"><span class="page-link">{{ $numberPage }}</span></li>
                        @else
                            <li class="page-item"><a class="page-link" href="{{ $url }}">{{ $numberPage }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}">&raquo;</a></li>
            @else
                <li class="page-item"><span class="page-link disable">&raquo;</span></li>
            @endif
        </ul>
    </div>
@endif
