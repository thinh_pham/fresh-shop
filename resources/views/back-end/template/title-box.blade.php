@php
$btnAction = sprintf(
'<a href="%s" class="btn btn-info"><i class="fas fa-plus mr-2"></i>Add new</a>',
route($controller.'-form')
);

if (!$pageIndex)
$btnAction = sprintf(
'<a href="%s" class="btn btn-danger"><i class="fas fa-backward mr-2"></i>Back</a>',
route($controller)
);
@endphp

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-1">
            <div class="col-sm-6 d-flex align-items-center">
                @if ($controller !== 'dashboard')
                    <h1 class="m-0">
                        {!! $btnAction !!}
                    </h1>
                @endif
            </div><!-- /.col -->
            <div class="col-sm-6 d-flex align-items-center flex-row-reverse">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                    <li class="breadcrumb-item active"><strong>{{ ucwords($controller) }}</strong></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
