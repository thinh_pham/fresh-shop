@php
use App\Helpers\Template as Template;
use App\Helpers\Form as FormTemplate;

$formLabelClass = config('custom.template.form_label.class');
$formInputClass = config('custom.template.form_input_text.class');

$statusValue = [
'default' => 'Select status',
'active' => config('custom.template.status.active.name'),
'inactive' => config('custom.template.status.inactive.name')
];

$inputHiddenID = Form::hidden('id', isset($item) ? $item->id : '');

$elements = [
// name
[
'label' => Form::label('name', 'Name', ['class' => $formLabelClass]),
'input' => Form::text(
'name',
isset($item) ? $item->name : '',
[
'class' => $formInputClass,
'placeholder' => 'Enter Name',
// 'autocomplete' => 'off'
]
)
],
// status
[
'label' => Form::label('status', 'Status', ['class' => $formLabelClass]),
'input' => Form::select(
'status',
$statusValue,
isset($item) ? $item->status : 'default',
['class' => $formInputClass . ' select2']
)
],
[
'type' => 'btn-submit',
'input' => $inputHiddenID . Form::button(
'<i class="fas fa-save mr-2"></i>Save',
[
'type' => 'submit',
'class' => 'btn btn-primary offset-sm-3'
]
)
],
];

@endphp

@extends('back-end.main')
@section('page-title')
    Freshshop - Admin - Form
@endsection
@section('page-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('back-end.template.title-box', ['pageIndex' => false])
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @include('back-end.template.error')
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Form</h3>
                            </div>
                            <!-- ./card-header -->
                            {{ Form::open([
                                    'url' => route($controller . '-save'),
                                    'method' => 'POST',
                                    'accept-charset' => 'UTF-8',
                                    'enctype' => 'multipart/form-data',
                                    'class' => 'form-horizontal',
                                ]) }}

                            <!-- card-body -->
                            {!! FormTemplate::show($elements) !!}
                            <!-- /.card-body -->
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
@endsection
