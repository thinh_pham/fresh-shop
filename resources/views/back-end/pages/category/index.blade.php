@php
use App\Helpers\Template as Template;
$filterButtons = Template::showFilterButtons($controller, $itemsByStatus, $params);
$searchArea = Template::showSearchArea($controller, $params['search']);
@endphp

@extends('back-end.main')
@section('page-title')
    Freshshop - Admin - Category
@endsection
@section('page-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('back-end.template.title-box', ['pageIndex' => true])
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">{!! $filterButtons !!}</div>
                                <div class="card-tools">
                                    {!! $searchArea !!}
                                    {{-- @include('back-end.pages.category.search')
                                    --}}
                                </div>
                            </div>
                            <!-- ./card-header -->
                            <div class="card-body">
                                <!-- notify -->
                                @include('back-end.template.notify')
                                <!-- table -->
                                @include('back-end.pages.category.list')
                            </div>
                            <!-- /.card-body -->
                            <!-- Box Pagination -->
                            @if (count($items) > 0)
                                {{ $items->appends(request()->input())->links('back-end.template.pagination', ['paginator' => $items]) }}
                            @endif
                            <!-- /.box-pagination -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
