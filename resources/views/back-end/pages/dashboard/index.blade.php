@extends('back-end.main')
@section('page-title')
    Freshshop - Admin - Dashboard
@endsection

@section('page-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('back-end.template.title-box', ['pageIndex' => 'true'])
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                @include('back-end.pages.dashboard.info-box')
                <!-- /.row -->
                <!-- Main row -->
                <div class="row">
                    <!-- /.Left col -->
                    <!-- right col (We are only adding the ID to make the widgets sortable)-->
                    <section class="col-lg-12 connectedSortable">

                        <!-- TO DO List -->
                        @include('back-end.pages.dashboard.todo-list')
                        <!-- /.card -->
                    </section>
                    <!-- right col -->
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
