<div class="row">
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><strong>New Order</strong></span>
                <span class="info-box-number">1,410</span>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="info-box">
            <span class="info-box-icon bg-success"><i class="fas fa-user-plus"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><strong>User Registrations</strong></span>
                <span class="info-box-number">410</span>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="info-box">
            <span class="info-box-icon bg-gradient-warning"><i class="fas fa-upload" style="color: #fff;"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><strong>Uploads</strong></span>
                <span class="info-box-number">13,648</span>
            </div>
        </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
        <!-- small box -->
        <div class="info-box">
            <span class="info-box-icon bg-gradient-danger"><i class="fas fa-door-open"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><strong>Unique Visitors</strong></span>
                <span class="info-box-number">13,648</span>
            </div>
        </div>
    </div>
    <!-- ./col -->
</div>
