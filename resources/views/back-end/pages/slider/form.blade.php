@php
use App\Helpers\Template as Template;
use App\Helpers\Form as FormTemplate;

$formLabelClass = config('custom.template.form_label.class');
$formInputClass = config('custom.template.form_input_text.class');

$statusValue = [
    'default' => 'Select status',
    'active' => config('custom.template.status.active.name'),
    'inactive' => config('custom.template.status.inactive.name')
];

$inputHiddenID = Form::hidden('id', isset($item) ? $item->id : '');
$inputHiddenThumb = Form::hidden('current-thumb', isset($item) ? $item->thumb : '');

$elements = [
    [
        'label' => Form::label('name', 'Name', ['class' => $formLabelClass]),
        'input' => Form::text(
            'name',
            isset($item) ? $item->name : '',
            [
                'class' => $formInputClass,
                'placeholder' => 'Enter Name',
                // 'autocomplete' => 'off'
            ]
        )
    ],
    [
        'label' => Form::label('description', 'Description', ['class' => $formLabelClass]),
        'input' => Form::textarea(
            'description',
            isset($item) ? $item->description : '',
            [
                'class' => $formInputClass . ' rsize-none',
                'rows' => 2,
                'placeholder' => 'Enter Description',
                // 'autocomplete' => 'off'
            ]
        )
    ],
    [
        'label' => Form::label('status', 'Status', ['class' => $formLabelClass]),
        'input' => Form::select(
            'status',
            $statusValue,
            isset($item) ? $item->status : 'default',
            ['class' => $formInputClass . ' select2']
        )
    ],
    [
        'label' => Form::label('link', 'Link', ['class' => $formLabelClass]),
        'input' => Form::text(
            'link',
            isset($item) ? $item->link : '',
            [
                'class' => $formInputClass,
                'placeholder' => 'Enter Link',
                // 'autocomplete' => 'off'
            ]
        )
    ],
    [
        'type' => 'thumb',
        'label' => Form::label('thumb', 'Thumbnail', ['class' => $formLabelClass]),
        'input' => Form::file('thumb', ['class' => 'custom-input-file', 'aria-describedby' => 'fileaddon']),
        'thumb' => isset($item) ? Template::showItemImage($controller, $item->thumb, $item->name) : null,
    ],
    [
        'type' => 'btn-submit',
        'input' => $inputHiddenID . $inputHiddenThumb . Form::button(
            '<i class="fas fa-save mr-2"></i>Save',
            [
                'type' => 'submit',
                'class' => 'btn btn-primary offset-sm-3'
            ]
        )
    ],
];

@endphp

@extends('back-end.main')
@section('page-title')
    Freshshop - Admin - Form
@endsection
@section('page-content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        @include('back-end.template.title-box', ['pageIndex' => false])
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                @include('back-end.template.error')
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Form</h3>
                            </div>
                            <!-- ./card-header -->
                            {{ Form::open([
                                    'url' => route($controller . '-save'),
                                    'method' => 'POST',
                                    'accept-charset' => 'UTF-8',
                                    'enctype' => 'multipart/form-data',
                                    'class' => 'form-horizontal',
                                ]) }}

                            <!-- card-body -->
                            {!! FormTemplate::show($elements) !!}
                            <!-- /.card-body -->
                            {{ Form::close() }}
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    </div>
@endsection
