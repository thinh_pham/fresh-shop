@php
use App\Helpers\Template as Template;
use App\Helpers\Hightlight as Hightlight;
$index = 0;
@endphp

<table class="table table-bordered table-hover">
    <thead class="bg-dark">
        <tr class="text-center">
            <th>#</th>
            <th>Thumbnail</th>
            <th>Info</th>
            <th>Status</th>
            <th>Created</th>
            <th>Modified</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
        @if (count($items) > 0)
            @foreach ($items as $item)
                @php
                $index = $index + 1;
                $name = Hightlight::show($item['name'],'name', $params['search']);
                $description = Hightlight::show($item['description'],'description', $params['search']);
                $link = Hightlight::show($item['link'],'link', $params['search']);
                $thumb = Template::showItemImage($controller, $item['thumb'], $item['name']);
                $status = Template::showItemStatus($controller, $item['status'], $item['id']);
                $createdHistory = Template::showItemHistory($item['created_by'], $item['created'], 'created_by',
                $params['search']);
                $modifiedHistory = Template::showItemHistory($item['modified_by'], $item['modified'], 'modified_by',
                $params['search']);
                $optionButtons = Template::showItemOptions($controller, $item['id']);
                @endphp

                <tr data-widget="expandable-table" aria-expanded="false">
                    <td class="text-center">{{ $index }}</td>
                    <td width="15%">{!! $thumb !!}</td>
                    <td width="28%">
                        <p><strong>Name:</strong> {!! $name !!}</p>
                        <p><strong>Link:</strong> {!! $link !!}</p>
                        <p><strong>Description:</strong> {!! $description !!}</p>
                    </td>
                    <td class="text-center">
                        {!! $status !!}
                    </td>
                    <td>{!! $createdHistory !!}</td>
                    <td>{!! $modifiedHistory !!}</td>
                    <td>{!! $optionButtons !!}</td>
                </tr>
                {{-- <tr class="expandable-body">
                    <td colspan="8">
                        <p><strong>Description:</strong> {{ $description }}</p>
                    </td>
                </tr> --}}
            @endforeach
        @else
            @include('back-end.template.empty', ['colspan' => 8])
        @endif
    </tbody>
</table>
