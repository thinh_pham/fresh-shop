<?php

return [
    'url' => [
        'prefix' => [
            'server' => [
                'admin' => 'admin',
                'dashboard' => 'dashboard',
                'slider' => 'slider',
                'category' => 'category',
            ],
            'client' => [
                'home' => '',
                'about' => 'about',
                'shop' => 'shop',
                'gallery' => 'gallery',
            ],
        ],
    ],
    'format' => [
        'long_time' => 'H:m:s d-m-Y',
        'short_time' => 'd-m-Y',
    ],
    'template' => [
        'form_label' => ['class' => 'col-sm-2 col-form-label d-flex flex-row-reverse'],
        'form_input_text' => ['class' => 'col-sm-7 offset-sm-1 form-control-sm'],
        'form_input_file' => ['class' => 'col-sm-7 offset-sm-1 form-control-sm p-0'],
        'status' => [
            'all' => ['name' => 'All', 'class' => 'btn-primary'],
            'active' => ['name' => 'Active', 'class' => 'btn-success'],
            'inactive' => ['name' => 'Inactive', 'class' => 'btn-danger'],
            'default' => ['name' => 'Undefined', 'class' => 'btn-default'],
        ],
        'is_home' => [
            'show' => ['name' => 'Show', 'class' => 'btn-success'],
            'hide' => ['name' => 'Hide', 'class' => 'btn-danger'],
        ],
        'options' => [
            'view' => [
                'title' => 'View',
                'class' => 'btn-info',
                'icon' => 'fa-eye',
                'route-name' => '-form',
            ],
            'edit' => [
                'title' => 'Edit',
                'class' => 'btn-success',
                'icon' => 'fa-pencil-alt',
                'route-name' => '-form',
            ],
            'delete' => [
                'title' => 'Delete',
                'class' => 'btn-danger btn-delete',
                'icon' => 'fa-trash',
                'route-name' => '-delete',
            ],
        ],
        'search' => [
            'all' => ['name' => 'All'],
            'id' => ['name' => 'ID'],
            'name' => ['name' => 'Name'],
            'description' => ['name' => 'Description'],
            'status' => ['name' => 'Status'],
            'created_by' => ['name' => 'Created By'],
            'modified_by' => ['name' => 'Modified By'],
        ],
    ],
    'area' => [
        'options' => [
            'default' => ['edit', 'delete'],
            'slider' => ['edit', 'delete'],
            'category' => ['edit', 'delete'],
        ],
        'search' => [
            'default' => ['all', 'name', 'description', 'status'],
            'slider' => ['all', 'name', 'description', 'created_by', 'modified_by'],
            'category' => ['all', 'name', 'created_by', 'modified_by'],
        ],
    ],
];
