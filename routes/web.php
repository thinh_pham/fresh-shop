<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Client\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
/**
 * Route: CLIENT
 */
$prefixClient = config('custom.url.prefix.client.home', '');
Route::group(['prefix' => $prefixClient], function () {
    /**
     * Route: HOME
     */
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/about-us', [HomeController::class, 'index'])->name('about-us');
    Route::get('/shop', [HomeController::class, 'index'])->name('shop');
    Route::get('/gallery', [HomeController::class, 'index'])->name('gallery');
    Route::get('/contact-us', [HomeController::class, 'index'])->name('contact-us');

});

/**
 * Route: ADMIN
 */
$prefixAdmin = config('custom.url.prefix.server.admin', 'admin');
Route::group(['prefix' => $prefixAdmin], function () {
    /**
     * Route: DASHBOARD
     */
    $prefixAdmin_Dashboard = config('custom.url.prefix.server.dashboard', 'dashboard');
    Route::group(['prefix' => $prefixAdmin_Dashboard], function () {
        $controllerName = 'dashboard';

        Route::get(
            '/',
            [DashboardController::class, 'index']
        )->name($controllerName);
        Route::get(
            '/form/{id?}',
            [DashboardController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');

    });
    /**
     * Route: SLIDER
     */
    $prefixAdmin_Slider = config('custom.url.prefix.server.slider', 'slider');
    Route::group(['prefix' => $prefixAdmin_Slider], function () {
        $controllerName = 'slider';
        Route::get(
            '/',
            [SliderController::class, 'index']
        )->name($controllerName);
        Route::get(
            '/form/{id?}',
            [SliderController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');
        Route::post(
            '/save',
            [SliderController::class, 'save']
        )->name($controllerName . '-save');
        Route::get(
            '/delete/{id}',
            [SliderController::class, 'delete']
        )->where('id', '^[0-9]+$')->name($controllerName . '-delete');
        Route::get(
            '/change-status-{status}/{id}',
            [SliderController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-change-status');
    });
    /**
     * Route: CATEGORY
     */
    $prefixAdmin_Category = config('custom.url.prefix.server.category', 'category');
    Route::group(['prefix' => $prefixAdmin_Category], function () {
        $controllerName = 'category';
        Route::get(
            '/',
            [CategoryController::class, 'index']
        )->name($controllerName);
        Route::get(
            '/form/{id?}',
            [CategoryController::class, 'form']
        )->where('id', '^[0-9]+$')->name($controllerName . '-form');
        Route::post(
            '/save',
            [CategoryController::class, 'save']
        )->name($controllerName . '-save');
        Route::get(
            '/delete/{id}',
            [CategoryController::class, 'delete']
        )->where('id', '^[0-9]+$')->name($controllerName . '-delete');
        Route::get(
            '/change-status-{status}/{id}',
            [CategoryController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-change-status');
        Route::get(
            '/change-is-{is_home}/{id}',
            [CategoryController::class, 'status']
        )->where('id', '^[0-9]+$')->name($controllerName . '-is-home');
    });
});
