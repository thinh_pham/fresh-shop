const btnSearch = document.querySelector("#btn-search");
const btnClear = document.querySelector("#btn-clear-search");
const btnsDelete = document.querySelectorAll(".btn-delete");
const btnActiveField = document.querySelector("button.btn-active-field");
const inputSearchField = document.querySelector("input[name=search_field]");
const inputSearchValue = document.querySelector("input[name=search_value]");
const selectFields = document.querySelectorAll("a.select-field");

selectFields.forEach(selectField => {
    selectField.addEventListener("click", e => {
        e.preventDefault();

        let field = selectField.dataset.field;
        let fieldName = selectField.innerHTML;
        btnActiveField.innerHTML = fieldName + ' <span class="caret"></span>';
        inputSearchField.value = field;
    });
});

if (btnSearch && btnClear) {
    btnSearch.addEventListener("click", () => {
        let searchField = inputSearchField.value;
        let searchValue = inputSearchValue.value;
        let params = ["filter_status"];
        let searchParams = new URLSearchParams(window.location.search);
        let link = "";
        params.forEach(param => {
            if (searchParams.has(param))
                link += param + "=" + searchParams.get(param) + "&";
        });

        window.location.href =
            "?" +
            link +
            "search_field=" +
            searchField +
            "&search_value=" +
            searchValue;
    });

    btnClear.addEventListener("click", () => {
        let path = window.location.pathname;
        let params = ["filter_status"];
        let searchParams = new URLSearchParams(window.location.search);
        let link = "";
        params.forEach(param => {
            if (searchParams.has(param))
                link += param + "=" + searchParams.get(param);
        });
        if (link !== "") window.location.href = "?" + link;
        else window.location.href = path;
    });
}

if (btnsDelete) {
    btnsDelete.forEach(btnDelete => {
        btnDelete.addEventListener("click", e => {
            if (!confirm("Do you really want to delete this element?")) {
                e.preventDefault();
                return false;
            }
        });
    });
}
